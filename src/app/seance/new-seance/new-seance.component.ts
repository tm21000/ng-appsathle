import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-seance',
  templateUrl: './new-seance.component.html',
  styleUrls: ['./new-seance.component.css']
})
export class NewSeanceComponent implements OnInit {

  constructor() { }

  seance = {
    label: "Reprise",
    date: "26/07/2021",
    exercices: [
      {
        categorie: "Technique",
        type: "Saut sable",
        dash: "2 - 4",
        quantite: "5 - 10"
      },
      {
        categorie: "Technique",
        type: "Saut plat dos tapis",
        dash: "2 - 4",
        quantite: "5 - 10"
      },
      {
        categorie: "Technique",
        type: "Saut complet",
        dash: "6 - 10",
        quantite: "5 - 10"
      },
      {
        categorie: "Course",
        type: "Vitesse",
        distance: "30",
        quantite: "3"
      },
      {
        categorie: "Course",
        type: "Haie jaune",
        distance: "20",
        quantite: "6",
        commentaire: "Avec perche"
      },
      {
        categorie: "Course",
        type: "Haie orange",
        distance: "20",
        quantite: "6",
        commentaire: "Avec latte au dessus de la tête"
      }
    ]

  }

  addline () {
    console.log("addline");
    let exercice = this.seance.exercices
    exercice.push({
      categorie: "Course",
      type: "",
      distance: "",
      quantite: "",
      commentaire: ""
    })
  }
  ngOnInit(): void {
  }

}
