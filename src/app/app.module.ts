import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SeanceComponent } from './seance/seance.component';
import { AccueilComponent } from './accueil/accueil.component';
import { NewSeanceComponent } from './seance/new-seance/new-seance.component';

@NgModule({
  declarations: [
    AppComponent,
    SeanceComponent,
    AccueilComponent,
    NewSeanceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
