export class Footing {
    distance: string = ""
    duration: string = ""
    surface: string = ""

    constructor(distance: string, duration: string, surface: string) {
        this.distance = distance;
        this.duration = duration;
        this.surface = surface;
    }
}

export class Technique {
    dash: string = ""
    duration?: string = ""
    surface: string = ""

    constructor(dash: string, duration: string, surface: string) {
        this.dash = dash;
        this.duration = duration;
        this.surface = surface;
    }
}